import { MongoClient } from 'mongodb';
import { Collection } from './Collection';
import { MongoDocument } from './helpers';

export class Connection {
  constructor(
    public readonly host: string,
    public readonly database: string,
    public readonly username: string = null,
    public readonly password: string = null,
    public readonly port: number = 27017,
  ) {
  }

  protected mongoClient = MongoClient.connect(this.createUrl(), {
    useNewUrlParser: true,
  }).then(client => {
    return client.db(this.database);
  });

  protected createUrl() {
    let auth = '';

    if (this.username !== null) {
      auth += this.username;
    }
    if (this.password !== null) {
      auth += `:${this.password}`;
    }
    if (auth !== '') {
      auth += '@';
    }

    return `mongodb://${this.host}:${this.port}`;
  }

  createCollection<T extends MongoDocument>(collectionName: string) {
    return new Collection<T>(this.mongoClient, collectionName);
  }

}

export default Connection;
