import { Binary } from 'bson';

export type Projection<T> = {
  [P in keyof T]?: boolean | 1 | 0;
}

export type Query<T> = {
  [P in keyof T]?: FieldQuery<T[P]>;
} & ExtendedQuery<T>;

type ExtendedQuery<T> = {
  $or?: Query<T>[];
  $and?: Query<T>[];
  $not?: Query<T>[];
  $nor?: Query<T>[];
};

type FieldQuery<T> = T | {
  $not?: FieldQuery<T>;
  $gt?: T;
  $lt?: T;
  $gte?: T;
  $lte?: T;
  $in?: T[];
  $ne?: T;
  $nin?: T[];
  $regex?: string | RegExp;
  $options?: string;
  $exists?: boolean;
  $mod?: [number, number];
  $text?: {
    $search: string;
    $language?: string;
    $caseSensitive?: boolean;
    $diactiricSensitive?: boolean;
  };
  $where?: Function;
  $all?: T[];
  $elemMatch?: FieldQuery<T>;
  $size?: number;
};

export type Update<T> = {
  $set?: Partial<T>;
  $inc?: UpdateMapper<T>;
  $min?: UpdateMapper<T, number>;
  $max?: UpdateMapper<T, number>;
  $mul?: UpdateMapper<T, number>;
  $push?: UpdateMapper<T>;
} & Partial<T>;

type UpdateMapper<T, V = any> = {
  [P in keyof T]?: V;
};
export type IndexInfo<T> = {
  [P in keyof T]?: boolean | 'hashed' | 1 | -1;
};

export type UpdateOptions = {
  upsert?: boolean;
  w?: any;
  wtimeout?: number;
  j?: boolean;
};

export type DeleteOptions = {
  w?: number | string;
  wtimmeout?: number;
  j?: boolean;
  bypassDocumentValidation?: boolean;
};

export interface MongoDocument {
  id: Binary;
  added: Date;
  updated: Date;
}

export function binaryEquals(binA: Binary, binB: Binary) {
  return binA.buffer.equals(binB.buffer);
}
