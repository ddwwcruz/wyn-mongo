import { Buffer } from 'buffer';
import { Binary } from 'bson';
import * as uuidv4 from 'uuid/v4';
import * as uuidv5 from 'uuid/v5';

const UUID_BUFFER_LENGTH = 16;

export function generateUuid(
  name?: string | Array<any>,
) {
  let domain = Array.from(generateUuidBuffer());
  return new Binary(generateUuidWithName(name, domain), Binary.SUBTYPE_UUID);
}

function generateUuidBuffer() {
  let buffer = Buffer.alloc(UUID_BUFFER_LENGTH);
  uuidv4(null, buffer, 0);
  return buffer;
}

function generateUuidWithName(name: any, namespace: any) {
  let buffer = Buffer.alloc(UUID_BUFFER_LENGTH);
  uuidv5(name, namespace, buffer, 0);
  return buffer;
}

export {
  uuidv4,
  uuidv5,
};
