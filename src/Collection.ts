import {
  Db,
  IndexOptions,
  FindOneOptions,
  MongoCountPreferences,
  CollectionAggregationOptions,
} from 'mongodb';
import * as _ from 'lodash';
import {
  Query,
  IndexInfo,
  Update,
  DeleteOptions,
  UpdateOptions,
  MongoDocument,
} from './helpers';
import { generateUuid } from './uuid';

export class Collection<T extends MongoDocument> {
  /**
   * Usage: type QueryType = typeof collection.Query;
   */
  readonly Query: Query<T> = null;

  /**
   * Usage: type UpdateType = typeof collection.Update;
   */
  readonly Update: Update<T> = null;

  constructor(
    private client: Promise<Db>,
    private name: string
  ) {
  }

  generateId = () => generateUuid(this.name);

  protected async getCollecton() {
    const client = await this.client;
    return client.collection<T>(this.name);
  }

  async find(query: Query<T> = {}, options?: FindOneOptions) {
    const collection = await this.getCollecton();
    return collection.find(query, options);
  }

  async findArray(query: Query<T> = {}, options?: FindOneOptions) {
    const cursor = await this.find(query, options);
    return cursor.toArray();
  }

  async findOne(query: Query<T> = {}, options?: FindOneOptions) {
    const collection = await this.getCollecton();
    return collection.findOne(query, options)
  }

  /**
   * @deprecated
   */
  async count(query: Query<T> = {}, preferences?: MongoCountPreferences) {
    console.warn('function count is deprecated. Use countDocuments or eximatedDocumentCount instead');
    const collection = await this.getCollecton();
    return collection.countDocuments(query, preferences)
  }

  async countDocuments(query: Query<T> = {}, preferences?: MongoCountPreferences) {
    const collection = await this.getCollecton();
    return collection.countDocuments(query, preferences);
  }

  async estimatedDocumentCount(query: Query<T> = {}, preferences?: MongoCountPreferences) {
    const collection = await this.getCollecton();
    return collection.estimatedDocumentCount(query, preferences);
  }

  async insert(values: Partial<T>): Promise<T> {
    const collection = await this.getCollecton();
    const id = this.generateId();
    const added = new Date();
    const updated = new Date();
    const insert = _.merge(values, { id, added, updated });
    await collection.insert(insert);

    return insert as T;
  }

  async update(query: Query<T>, update: Update<T>, options?: UpdateOptions) {
    const collection = await this.getCollecton();
    update.$set.updated = new Date();
    return collection.update(query, update, options);
  }

  async updateMany(query: Query<T>, update: Update<T>, options?: UpdateOptions) {
    const collection = await this.getCollecton();
    update.$set.updated = new Date();
    return collection.updateMany(query, update, options);
  }

  async updateOne(query: Query<T>, update: Update<T>, options?: UpdateOptions) {
    const collection = await this.getCollecton();
    update.$set.updated = new Date();
    return collection.updateOne(query, update, options);
  }

  async deleteMany(query: Query<T>, options?: DeleteOptions) {
    const collection = await this.getCollecton();
    return collection.deleteMany(query, options);
  }

  async deleteOne(query: Query<T>, options?: DeleteOptions) {
    const collection = await this.getCollecton();
    return collection.deleteOne(query, options);
  }

  async createIndex(fields: IndexInfo<T>, options?: IndexOptions) {
    const collection = await this.getCollecton();
    return collection.createIndex(fields, options);
  }

  async distinct<D>(key: string, query?: Query<T>): Promise<D> {
    const collection = await this.getCollecton();
    return collection.distinct(key, query);
  }

  async aggregate<D>(pipeline: any[], options?: CollectionAggregationOptions) {
    const collection = await this.getCollecton();
    return collection.aggregate<D>(pipeline, options);
  }
}
